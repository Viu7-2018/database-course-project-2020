﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProjectApp.Models
{
    public class Attribute
    {
        public int Id { get; set; }
        //public int AttributeNameId { get; set; }

        public virtual ICollection<AttributeGroup> AttributeGroups { get; set; }


        
    }
}
