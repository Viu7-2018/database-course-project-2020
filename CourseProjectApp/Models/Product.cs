using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace CourseProjectApp.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a product name")]
        public string Name { get; set; }


        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Brand> Brands { get; set; }


    }
}
