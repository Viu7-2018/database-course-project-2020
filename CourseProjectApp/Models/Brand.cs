﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CourseProjectApp.Models
{
    public class Brand
    {
        
        public int Id { get; set; }
        public string BrandName { get; set; }

        public int? BrandId { get; set; }
        public virtual Product Product { get; set; }
    }
}
