﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProjectApp.Models
{
    public class LocalAttributeGroupeName
    {
        public int Id { get; set; }

        public string AttributeGroupeName { get; set; }

        public int? AttribueGroupId { get; set; }
        public virtual AttributeGroup AttributeGroup { get; set; }
    }
}
