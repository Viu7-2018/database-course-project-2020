﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProjectApp.Models
{
    public class LocalAttributeName
    {
        
        public int Id { get; set; }

        public string AttributeName { get; set; }

        public int? AttributeId { get; set; }
        public virtual Attribute Attribute { get; set; }
    }
}
