﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace CourseProjectApp.Models
{
    public class Image 
    {
        [Key]
        public int Id { get; set; }
        public string Img { get; set; }

        public int? ProductId { get; set; }
        public virtual Product Product{ get; set; }
    }


}

