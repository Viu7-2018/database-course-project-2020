using CourseProjectApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CourseProjectApp.Data
{
    public class CourseProjectAppContext : IdentityDbContext
    {
        public CourseProjectAppContext (
            DbContextOptions<CourseProjectAppContext> options)
            : base(options)
        {
        }
        
        public DbSet<Product> Products { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
        public DbSet<AttributeGroup> AttributeGroups { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<LocalAttributeGroupeName> LocalAttributeGroupeNames { get; set; }
        public DbSet<LocalAttributeName> LocalAttributeNames { get; set; }
        public DbSet<Locale> Locales { get; set; }

    }
}