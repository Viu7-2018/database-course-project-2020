using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace CourseProjectApp.Pages
{
    public class AttributesModel : PageModel
    {
        private readonly ILogger<AttributesModel> _logger;

        public AttributesModel(ILogger<AttributesModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}