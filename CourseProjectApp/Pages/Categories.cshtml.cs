using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace CourseProjectApp.Pages
{
    public class CategoriesModel : PageModel
    {
        private readonly ILogger<CategoriesModel> _logger;

        public CategoriesModel(ILogger<CategoriesModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}