﻿// <auto-generated />
using System;
using CourseProjectApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CourseProjectApp.Migrations
{
    [DbContext(typeof(CourseProjectAppContext))]
    [Migration("20210116145149_ConfigureCategory")]
    partial class ConfigureCategory
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("AttributeGroupCategory", b =>
                {
                    b.Property<int>("AttributeGroupsId")
                        .HasColumnType("integer");

                    b.Property<int>("CategoriesCategoryId")
                        .HasColumnType("integer");

                    b.HasKey("AttributeGroupsId", "CategoriesCategoryId");

                    b.HasIndex("CategoriesCategoryId");

                    b.ToTable("AttributeGroupCategory");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Attribute", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.HasKey("Id");

                    b.ToTable("Attributes");
                });

            modelBuilder.Entity("CourseProjectApp.Models.AttributeGroup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("AttributeId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("AttributeId");

                    b.ToTable("AttributeGroups");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Brand", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("BrandId")
                        .HasColumnType("integer");

                    b.Property<string>("BrandName")
                        .HasColumnType("text");

                    b.Property<int?>("ProductId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.ToTable("Brands");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Category", b =>
                {
                    b.Property<int>("CategoryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("CategoryName")
                        .HasColumnType("text");

                    b.Property<int?>("ParentCategoryId")
                        .HasColumnType("integer");

                    b.Property<int?>("ProductId")
                        .HasColumnType("integer");

                    b.HasKey("CategoryId");

                    b.HasIndex("ParentCategoryId");

                    b.HasIndex("ProductId");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Img")
                        .HasColumnType("text");

                    b.Property<int?>("ProductId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("CourseProjectApp.Models.LocalAttributeGroupeName", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("AttribueGroupId")
                        .HasColumnType("integer");

                    b.Property<int?>("AttributeGroupId")
                        .HasColumnType("integer");

                    b.Property<string>("AttributeGroupeName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("AttributeGroupId");

                    b.ToTable("LocalAttributeGroupeNames");
                });

            modelBuilder.Entity("CourseProjectApp.Models.LocalAttributeName", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("AttributeId")
                        .HasColumnType("integer");

                    b.Property<string>("AttributeName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("AttributeId");

                    b.ToTable("LocalAttributeNames");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Locale", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.HasKey("Id");

                    b.ToTable("Locales");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("AttributeGroupCategory", b =>
                {
                    b.HasOne("CourseProjectApp.Models.AttributeGroup", null)
                        .WithMany()
                        .HasForeignKey("AttributeGroupsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CourseProjectApp.Models.Category", null)
                        .WithMany()
                        .HasForeignKey("CategoriesCategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("CourseProjectApp.Models.AttributeGroup", b =>
                {
                    b.HasOne("CourseProjectApp.Models.Attribute", null)
                        .WithMany("AttributeGroups")
                        .HasForeignKey("AttributeId");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Brand", b =>
                {
                    b.HasOne("CourseProjectApp.Models.Product", "Product")
                        .WithMany("Brands")
                        .HasForeignKey("ProductId");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Category", b =>
                {
                    b.HasOne("CourseProjectApp.Models.Category", "ParentCategory")
                        .WithMany()
                        .HasForeignKey("ParentCategoryId");

                    b.HasOne("CourseProjectApp.Models.Product", null)
                        .WithMany("Categories")
                        .HasForeignKey("ProductId");

                    b.Navigation("ParentCategory");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Image", b =>
                {
                    b.HasOne("CourseProjectApp.Models.Product", "Product")
                        .WithMany("Images")
                        .HasForeignKey("ProductId");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("CourseProjectApp.Models.LocalAttributeGroupeName", b =>
                {
                    b.HasOne("CourseProjectApp.Models.AttributeGroup", "AttributeGroup")
                        .WithMany("LocalAttributeGroupeNames")
                        .HasForeignKey("AttributeGroupId");

                    b.Navigation("AttributeGroup");
                });

            modelBuilder.Entity("CourseProjectApp.Models.LocalAttributeName", b =>
                {
                    b.HasOne("CourseProjectApp.Models.Attribute", "Attribute")
                        .WithMany()
                        .HasForeignKey("AttributeId");

                    b.Navigation("Attribute");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Attribute", b =>
                {
                    b.Navigation("AttributeGroups");
                });

            modelBuilder.Entity("CourseProjectApp.Models.AttributeGroup", b =>
                {
                    b.Navigation("LocalAttributeGroupeNames");
                });

            modelBuilder.Entity("CourseProjectApp.Models.Product", b =>
                {
                    b.Navigation("Brands");

                    b.Navigation("Categories");

                    b.Navigation("Images");
                });
#pragma warning restore 612, 618
        }
    }
}
