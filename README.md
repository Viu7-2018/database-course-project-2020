# Database Course Project 2020
Проект представляет собой монолит на ASP.NET Core с использованием Razor Pages и EntityFramework Core.

## Запуск проекта
#### Пререквизиты
- .NET 5.0 SDK  ([тык](https://dotnet.microsoft.com/download))
- PostgreSQL 13 ([тык](https://www.postgresql.org/download/)).
- Visual Studio Code ([тык](https://code.visualstudio.com/download))

#### NB! Перед первым запуском
- Надо установить доверие к сертификату разработки HTTPS:
```dotnet dev-certs https --trust```

#### Консольные команды для запуска
- ```dotnet run``` - запускает дев версию проекта. Доступно по адресу https://localhost:4000
- ```dotnet watch run``` - запуск проекта с вотчером изменений
- ```dotnet build``` - сборка проекта

#### Гит:
Как получить обновления из master в свою ветку:
- заходим в Git Bash-е в нашу папку с проектом
- находясь на своей ветке выполняем: ```git rebase master```

#### Докер:
- ```docker run --name pg -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres``` - создаем контейнер, вместо pg может быть любое имя
- ```docker ps -a``` - список контейнеров
- ```docker stop pg``` - остановить контейнер pg
- ```docker start pg``` - запустить контейнер pg

#### Миграции:
1) Удалить ВСЕ таблицы, если они у вас ранее создавались для данной БД, иначе будет ошибка relation %name% already exists:
- ```drop table "AttributeGroupCategory" , "AttributeGroups" , "Attributes" , "Brands" , "Categories", "CategoryNames" , "Images" , "LocalAttributeGroupeNames","LocalAttributeNames" ,"Locales" ,"Products" , "Users", "__EFMigrationsHistory";```
2) Visual Studio заходим в PM>: 
- ```Средства > Диспетчер пакетов NuGet > Консоль диспетчера пакетов```
3) Создаем миграцию PM>:
- ```add-migration AddUser``` - гдеAddUser просто имя миграции, в данном примере создается таблица User, а Add говорит о том что это её добавление(а не удаление или её изменение)
4) Применяем миграцию PM>:
- ```update-database``` 
5) Отменить миграцию PM>:
- ```remove-migration``` 

#### Консольные команды для создания/применения миграций
- ```dotnet ef migrations add SomeNameForYourMigration``` - создает миграцию в папке Migrations с именем SomeNameForYourMigration
- ```dotnet ef database update``` - применяет миграцию 

#### Консольные команды для создания/применения миграций
- ```dotnet ef migrations add SomeNameForYourMigration``` - создает миграцию в папке Migrations с именем SomeNameForYourMigration
- ```dotnet ef database update``` - применяет миграцию 

## Полезные ссылки
- Документация по ASP.NET ([тык](https://docs.microsoft.com/ru-ru/aspnet/core/?view=aspnetcore-5.0))
- Документация по Razor Pages ([тык](https://docs.microsoft.com/ru-ru/aspnet/core/tutorials/razor-pages/?view=aspnetcore-5.0))
- Документаци по EF Core ([тык](https://docs.microsoft.com/ru-ru/ef/))
